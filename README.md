# Simple Neuro Evolution

This projects implements a basic neural network evolution mechanism using genetic algorithm.

A population of n clients is created and fed data for training.

The fittest clients are kept, crossed over and mutated to renew the population at each iteration (or generation).

It is tested with a simple XOR prediction.

To execute the project, open a terminal at the root folder and run

`npm start`

You can dynamically choose the number of clients and number of generations from command line

`npm start <clients> <generations>`