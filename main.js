import { Population } from './src/population.js'

function testXOR(clients, generations) {
    const dataset = {
        x: [[0, 0], [0, 1], [1, 0], [1, 1]],
        y: [[0], [1], [1], [0]]
    }
    const topology = [2,5,1];
    
    let population = new Population(topology, clients, 0.2);
    population.evolve(dataset, generations);
    
    let best = population.getBestClient();

    for (let input of dataset.x) {
        let result = best.think(input)[0];
        console.log(`Prediction for input ${input}: ${result}`)
    }
}

function main() {
    let clients, generations;
    if (process.argv.length == 4) {
        clients = +process.argv[2];
        generations = +process.argv[3];
    } else {
        clients = 100;
        generations = 100;
    }

    testXOR(clients, generations);
}

main();