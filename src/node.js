class Node {
    constructor(activation) {
        this.bias = Math.random() * 2 - 1;
        this.activation = activation;
        this.value;
    }
}

export { Node };
