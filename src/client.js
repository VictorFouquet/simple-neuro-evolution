import { Network } from './network.js';

class Client {
    constructor(topology) {
        this.network = new Network(...topology);
        this.fitness = 0;
    }

    train(dataset, fitness) {
        let results = []
        for (let input of dataset.x) {
            results.push(this.think(input))
        }

        this.fitness = fitness(results, dataset.y)
    }

    think(input) {
        return this.network.forward(input);
    }
}

export { Client };
