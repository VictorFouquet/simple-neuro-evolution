class Activation {
    static logistic(x) { return x; }

    static sigmoid(x) { return 1.0 / (1 + Math.exp(-x)); }

    static relu(x) { return Math.max(0,x); }

    static minMax(x) { return x >= 0.5 ? 1 : 0 }
}

export { Activation };
