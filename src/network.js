import { Node } from './node.js';
import { Layer } from './layer.js';
import { Activation } from './activation.js';

class Network {
    constructor(...nodes) {
        this.layers = [];

        this.createInputLayer(nodes[0]);
        this.createHiddenLayers(nodes);
        this.createOutputLayer(nodes[nodes.length - 1], nodes[nodes.length - 2]);
    }

    createInputLayer(n) {
        let inputs = new Layer();
        for (let i = 0; i < n; i++) {
            let node = new Node(Activation.logistic);
            node.bias = 0;

            inputs.nodes.push(new Node(Activation.logistic));
            inputs.inWeights.push([1]);
        }
        this.layers.push(inputs);
    }

    createHiddenLayers(nodes) {
        for (let i = 1; i < nodes.length - 1; i++) {
            let hidden = new Layer();
            for (let j = 0; j < nodes[i]; j++) {
                hidden.nodes.push(new Node(Activation.relu));
                let inWeights = [];
                for (let k = 0; k < nodes[i - 1]; k++) {
                    inWeights.push(Math.random() * 2 - 1);
                }
                hidden.inWeights.push(inWeights);
            }
            this.layers.push(hidden);
        }
    }

    createOutputLayer(n, previous) {
        let output = new Layer()
        for (let i = 0; i < n; i++) {
            output.nodes.push(new Node(Activation.sigmoid));
            let inWeights = [];
            for (let j = 0; j < previous; j++) {
                inWeights.push(Math.random() * 2 - 1);
            }
            output.inWeights.push(inWeights)
        }
        this.layers.push(output);
    }

    forward(inputs) {
        for (let i = 0; i <  this.layers[0].nodes.length; i++) {
            let node = this.layers[0].nodes[i];
            node.value = node.activation(inputs[i] + node.bias)
        }
        let currentIn = this.layers[0].nodes.map(n => n.value);

        // Iterates over layers
        for (let n = 1; n < this.layers.length; n++) {
            let layer = this.layers[n]
            // Iterate over the layer's nodes
            for (let i = 0; i < layer.nodes.length; i++) {
                let node = layer.nodes[i];
                let weightedSum = 0;
                // Iterate over the inputs coming from previous layer
                for (let j = 0; j < layer.inWeights[i].length; j++) {
                    weightedSum += layer.inWeights[i][j] * currentIn[j];
                }
                node.value = node.activation(node.bias + weightedSum);
            }
            currentIn = layer.nodes.map(n => n.value);
        }

        return this.layers[this.layers.length - 1].nodes.map(n => n.value);
    }

    topology() {
        return this.layers.map(layer => layer.nodes.length);
    }
}

export { Network };
