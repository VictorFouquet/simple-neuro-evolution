import { Client } from './client.js';
import { Network } from './network.js';

class Population {
    constructor(topology, networks, selectionRate) {
        this.topology = topology;
        this.clients = [];
        this.selectionRate = selectionRate;

        for (let i = 0; i < networks; i++) {
            this.clients.push(new Client(this.topology));
        }
    };

    fitness(x, y) {
        let sum = 0
        for (let i = 0; i < x.length; i++) {
            for (let j = 0; j < x[i].length; j++) {
                sum += Math.pow((y[i][j] - x[i][j]), 2);
            }
        }
        return (1 - sum)
    }

    train(dataset) {
        for (let client of this.clients) {
            client.train(dataset, this.fitness);
        }
    }

    mutate() {
        for (let client of this.clients) {
            for (let i = 1; i < client.network.layers.length; i++) {
                let layer = client.network.layers[i];
                for (let node of layer.nodes) {
                    if (Math.random() < 0.5) {
                        node.bias *= 1 + (Math.random() > 0.5 ? 0.1 : -0.1);
                    }
                }
    
                for (let inWeights of layer.inWeights) {
                    for (let i = 0; i < inWeights.length; i++) {
                        if (Math.random() < 0.5) {
                            inWeights[i] *= 1 + (Math.random() > 0.5 ? 0.1 : -0.1);
                        }
                    }
                }
            }
        }
    }

    crossOver(client1, client2) {
        let net1 = client1.network;
        let net2 = client2.network;
        let net = new Network(...net1.topology());

        for (let i = 0; i < net.layers.length; i++) {
            for (let j = 0; j < net.layers[i].nodes.length; j++) {
                net.layers[i].nodes[j].bias = (Math.random() > 0.5 ? net1 : net2).layers[i].nodes[j].bias;
            }
            for (let j = 0; j < net.layers[i].inWeights.length; j++) {
                for (let k = 0; k < net.layers[i].inWeights[j].length; k++) {
                    net.layers[i].inWeights[j][k] = (Math.random() > 0.5 ? net1 : net2).layers[i].inWeights[j][k];
                }
            }
        }

        let child = new Client(this.topology);
        child.network = net;

        return child;
    }

    repopulate() {
        let fittestPop = this.clients
            .sort((a,b) => b.fitness - a.fitness)
            .slice(0, Math.floor(this.clients.length * this.selectionRate));
        let newPop = [];

        while (newPop.length < this.clients.length) {
            let child = this.crossOver(
                fittestPop[Math.floor(Math.random() * fittestPop.length)],
                fittestPop[Math.floor(Math.random() * fittestPop.length)]
            )
            newPop.push(child)
        }

        this.clients = newPop;
    }

    getBestClient() {
        return this.clients.sort((a,b) => b.fitness - a.fitness)[0];
    }

    evolve(dataset, nGenerations) {       
        for (let i = 0; i < nGenerations; i++) {
            this.train(dataset);
            this.repopulate();
            this.mutate();
        }
    }
}

export { Population };
